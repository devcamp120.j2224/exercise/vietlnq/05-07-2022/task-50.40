package com.devcamp.j01_javabasic.s50;

public class Voucher {
    public String voucherCode = "V000000";

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public void showVoucher() {
        System.out.println("Voucher code is: " + this.voucherCode);
    }

    public static void main(String[] args) throws Exception {
        Voucher voucher = new Voucher();
        //voucher.showVoucher();

        // 1.
        // voucher.showVoucher("CODE****");

        // 2. 
        // voucher.voucherCode = "CODE2222";
        // voucher.showVoucher("0008888");

        String code = "VOUCHER";
        voucher.setVoucherCode(code);
        // System.out.println(voucher.getVoucherCode());
        // voucher.showVoucher(voucher.getVoucherCode());
        voucher.showVoucher();
    }
}
